# Dotfiles

Dotfiles are text files used for configuration of software, the *dot* refers to Unix way of hiding files by prepending a "." (dot) at the start of the name.

Keeping these dotfiles saved is a good practice, since all you have to do is just copy all these files back, in case of any inconvenience.

Keep in mind that these files are personal and are not meant to be used by anyone else in it's entirety, because of the specific workflow attached to it. They are here for **reference** for any user who comes across any interesting files or scripts.

In this repo there will some brief and objective documentation on specifics of these files.

## Main Setup

Some of the the software that i use.

* OS - [Debian](https://debian.org) x86_64
* Desktop - [jwm](https://joewing.net/projects/jwm/)
* Browser - [librewolf](https://librewolf), firefox
* Text Editor - [neovim](https://neovim.io)
* Terminal Emulator - [st](https://st.suckless.org), from [here](https://github.com/lukesmithxyz/st)
* Shell - [zsh](https://zsh.org)
* File Manager - [lf](https://github.com/gokcehan/lf), [pcmanfm](http://sourceforge.net/projects/pcmanfm)
* Music - [mpd](https://www.musicpd.org/) (server), [ncmpcpp](https://github.com/ncmpcpp/ncmpcpp) (client)
* News Reader(RSS) - [newsboat](https://newsboat.org)

Other apps can be found in `.local/share/doc/archlinux/*packages`

### wmutils setup

* TODO

### Linux tty/framebuffer setup

* TODO
