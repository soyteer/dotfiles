let mapleader ="\<Space>"

if ! filereadable(system('echo -n "${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim"'))
	echo "Downloading junegunn/vim-plug to manage plugins..."
	silent !mkdir -p ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/
	silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > ${XDG_CONFIG_HOME:-$HOME/.config}/nvim/autoload/plug.vim
	autocmd VimEnter * PlugInstall
endif

call plug#begin('~/.config/nvim/plugged')
Plug 'junegunn/goyo.vim'
Plug 'vimwiki/vimwiki'
Plug 'bling/vim-airline'
" Plug 'rrethy/vim-hexokinase', { 'do': 'make hexokinase' }
Plug 'mhinz/vim-startify'
Plug 'morhetz/gruvbox'
Plug 'tpope/vim-commentary'
Plug 'norcalli/nvim-colorizer.lua'
" Plug 'ervandew/supertab'
Plug 'alvan/vim-closetag'
call plug#end()

" Some basics:
  nnoremap c "_c
  set nocompatible
  filetype plugin on
  syntax on
  set encoding=utf-8
  set number relativenumber
  set clipboard+=unnamedplus
  set nohlsearch
  set bg=dark
  set go=a
  set mouse=a
  set termguicolors
  set title
  set titlestring=nvim\ -\ %-25.55F%m titlelen=70
  set scrolloff=999
  colorscheme gruvbox
  " set nobackup
  " set noswapfile
  " set nowritebackup
  set undofile

" Enable autocompletion:
  set wildmode=longest,list,full

" Disables automatic commenting on newline:
  autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" " Vim Hexokinase
"   let g:Hexokinase_refreshEvents = ['InsertLeave']

"   let g:Hexokinase_optInPatterns = [
"   \     'full_hex',
"   \     'triple_hex',
"   \     'rgb',
"   \     'rgba',
"   \     'hsl',
"   \     'hsla'
"   \ ]

"   let g:Hexokinase_highlighters = ['backgroundfull']

" " Re-enable hexokinase on enter
"   autocmd VimEnter * HexokinaseTurnOn

" Vim Colorizer.lua
  lua require'colorizer'.setup()

" Closetags sets
  let g:closetag_emptyTags_caseSensitive = 1
  let g:closetag_filenames = '*.html,*.xhtml,*.phtml,*.xml'
  let g:closetag_filetypes = 'html,xhtml,phtml,xml'
  let g:closetag_shortcut = '>'

" Goyo plug-in makes text more readable when writing prose:
  map <silent><leader>g :Goyo \| set linebreak!<CR>

" Spell-check set to <leader>o, 'o' for 'orthography':
  map <leader>o :setlocal spell! spelllang=en_us<CR>
  map <leader>ç :setlocal spell! spelllang=pt_br<CR>

" Correct a word
  map <leader>c z=

" Splits open at the bottom and right.
  set splitbelow splitright

" 101 ways to limit yourself.
  set colorcolumn=101

" Tab Settings
  set tabstop=2 softtabstop=0 expandtab shiftwidth=2 smarttab

" Visualize indentation characters
  set listchars+=tab:\|\ ,space:·
  nnoremap <silent><leader>l :set list!<cr>

" Auto indent file
  nnoremap <F6> mqgg=G'qzz

" Actual Tab alias
  inoremap <C-Tab> <C-v>Tab

" Shortcutting split navigation, saving a key press:
  map <C-h> <C-w>h
  map <C-j> <C-w>j
  map <C-k> <C-w>k
  map <C-l> <C-w>l

" Resize splits
  map <C-Left> :vertical resize -5<CR>
  map <C-Right> :vertical resize +5<CR>
  map <C-Down> :resize -5<CR>
  map <C-Up> :resize +5<CR>

" Tabbing shortcuts
  nnoremap <Tab> gt
  nnoremap <A-Tab> gT
  nnoremap <silent> <A-t> :tabnew<CR>

" Pasting stuff without overwriting the selection
  noremap <Leader>p "0p
  noremap <Leader>P "0P
  vnoremap <Leader>p "0p

" Terminal mode bindings
  tnoremap <Esc> <C-\><C-n>
  tnoremap <C-H> <C-\><C-N><C-w>h
  tnoremap <C-J> <C-\><C-N><C-w>j
  tnoremap <C-K> <C-\><C-N><C-w>k
  tnoremap <C-L> <C-\><C-N><C-w>l
  inoremap <C-H> <C-\><C-N><C-w>h
  inoremap <C-J> <C-\><C-N><C-w>j
  inoremap <C-K> <C-\><C-N><C-w>k
  inoremap <C-L> <C-\><C-N><C-w>l
  nnoremap <C-H> <C-w>h
  nnoremap <C-J> <C-w>j
  nnoremap <C-K> <C-w>k
  nnoremap <C-L> <C-w>l

" Replace ex mode with gq
	map Q gq

" I keep pressing these accidentally so just map them
  command W w
  command Q q
  command X x
  command! Wq :wq
  command! W :w
  command! Q :q
  :cmap Q! q!

" Check file in shellcheck:
  map <leader>s :!clear && shellcheck %<CR>

" Replace all is aliased to Alt-S.
  map <leader>r :%s//g<Left><Left>

" " Compile document, be it groff/LaTeX/markdown/etc.
" 	map <leader>c :w! \| !compiler <c-r>%<CR>

" " Open corresponding .pdf/.html or preview
" 	map <leader>p :!opout <c-r>%<CR><CR>

" " Runs a script that cleans out tex build files whenever I close out of a .tex file.
" 	autocmd VimLeave *.tex !texclear %

" Ensure files are read as what I want:
  let g:vimwiki_ext2syntax = {'.Rmd': 'markdown', '.rmd': 'markdown','.md': 'markdown', '.markdown': 'markdown', '.mdown': 'markdown'}
  map <leader>v :VimwikiIndex
  let g:vimwiki_list = [{'path': '~/vimwiki', 'syntax': 'markdown', 'ext': '.md'}]
  autocmd BufRead,BufNewFile /tmp/calcurse*,~/.calcurse/notes/* set filetype=markdown
  autocmd BufRead,BufNewFile *.ms,*.me,*.mom,*.man set filetype=groff
  autocmd BufRead,BufNewFile *.tex set filetype=tex
  autocmd BufRead,BufNewFile *.as set filetype=cpp

" Save file as sudo on files that require root permission
  cnoremap w!! execute 'silent! write !sudo tee % >/dev/null' <bar> edit!

" Enable Goyo by default for mutt writting
  autocmd BufRead,BufNewFile /tmp/neomutt* let g:goyo_width=80
  autocmd BufRead,BufNewFile /tmp/neomutt* :Goyo
  autocmd BufRead,BufNewFile /tmp/neomutt* map ZZ :Goyo\|x!<CR>
  autocmd BufRead,BufNewFile /tmp/neomutt* map ZQ :Goyo\|q!<CR>

" Automatically deletes all trailing whitespace on save.
  autocmd BufWritePre * %s/\s\+$//e
  autocmd BufWritepre * %s/\n\+\%$//e

" When shortcut files are updated, renew bash and ranger configs with new material:
" autocmd BufWritePost files,directories !shortcuts
" Run xrdb whenever Xdefaults or Xresources are updated.
  autocmd BufWritePost *Xresources,*Xdefaults !xrdb -load %
" Update binds when sxhkdrc is updated.
  autocmd BufWritePost *sxhkdrc !pkill -USR1 sxhkd
  autocmd BufWritePost ~/src/dwmblocks/config.h !cd ~/src/dwmblocks/; sudo -H make clean PREFIX=/usr install && { killall -q dwmblocks;setsid dwmblocks & }
" autocmd BufWritePost *.ent ! read -r ans && [ "$ans" = "y" ] || [ "$ans" = "Y" ] && { ripent -import % }

" Turns off highlighting on the bits of code that are changed, so the line that is changed is highlighted but the actual text that has changed stands out on the line and is readable.
if &diff
  highlight! link DiffText MatchParen
endif

" Function for toggling the bottom statusbar:
let s:hidden_all = 1
function! ToggleHiddenAll()
    if s:hidden_all  == 0
        let s:hidden_all = 1
        set noshowmode
        set noruler
        set laststatus=0
        set noshowcmd
    else
        let s:hidden_all = 0
        set showmode
        set ruler
        set laststatus=2
        set showcmd
    endif
endfunction
nnoremap <silent><leader>h :call ToggleHiddenAll()<CR>
