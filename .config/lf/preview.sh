#!/bin/sh
draw() {
  ~/.config/lf/draw_img.sh "$@"
  exit 1
}

hash() {
  printf '%s/.cache/lf/%s' "$HOME" \
    "$(stat --printf '%n\0%i\0%F\0%s\0%W\0%Y' -- "$(readlink -f "$1")" | sha256sum | awk '{print $1}')"
}

cache() {
  if [ -f "$1" ]; then
    draw "$@"
  fi
}

getfilemime() {
  mimetype="$(file -Lb --mime-type -- "$file")"
  if [ "$mimetype" = "inode/symlink" ]; then
    file -Lb --mime-type -- "$(realpath "$file")"
  else
    file -Lb --mime-type -- "$file"
  fi
}

file="$1"
shift

if [ -n "$FIFO_UEBERZUG" ]; then
  case "$(getfilemime)" in
    image/*)
      draw "$file" "$@"
      ;;
    video/*)
      cache="$(hash "$file").jpg"
      cache "$cache" "$@"
      ffmpegthumbnailer -i "$file" -o "$cache" -s 0
      draw "$cache" "$@"
      ;;
    *x-rar|*x-7z-compressed|*zip|*debian.binary*)
      bsdtar -tf "$file" | sort
      ;;
    text/*|*/xml|*/json|*/x-shellscript)
      bat -pp -f --tabs 2 --theme "Monokai Extended Bright" "$file" ||
      cat "$file"
      ;;
    audio/*)
      mediainfo "$file"
      ;;
    *opendocument*)
      odt2txt "$file"
      ;;
    *octet-stream)
      hexdump -C -n 256 "$file"
      ;;
    *)
      echo '----- File Type Classification -----'
      file -b "$file" | sed 's/,[ ]/\n/g'
      ;;
  esac
fi

exit 0
