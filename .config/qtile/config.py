# -*- coding: utf-8 -*-
import os
import re
import socket
import subprocess
from libqtile.config import KeyChord, Key, Screen, Group, Match, Drag, Click
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
from libqtile.lazy import lazy
from typing import List  # noqa: F401

mod = "mod4"                                        # Sets mod key to SUPER/WINDOWS
myTerm = "st"                                       # My terminal of choice
myConfig = "/home/lucas/.config/qtile/config.py"    # The Qtile config file location
scriptfolder: str = "/home/lucas/.local/sbin/statusbar/"

keys = [
         ### The essentials
         Key([mod], "Return",
             lazy.spawn(myTerm),
             desc='Launches My Terminal'
             ),
         Key([mod], "r",
             lazy.spawn("dmenu_run_history -i"),
             # lazy.spawn("rofi -show drun -config ~/.config/rofi/themes/dt-dmenu.rasi -display-drun \"Run: \" -drun-display-format \"{name}\""),
             desc='Run Launcher'
             ),
         Key([mod], "Tab",
             lazy.next_layout(),
             desc='Toggle through layouts'
             ),
         Key([mod], "apostrophe",
             lazy.window.kill(),
             desc='Kill active window'
             ),
         Key([mod, "shift"], "r",
             lazy.restart(),
             desc='Restart Qtile'
             ),
         Key([mod, "shift"], "q",
             lazy.shutdown(),
             desc='Shutdown Qtile'
             ),
         # Key(["control", "shift"], "e",
         #     lazy.spawn("emacsclient -c -a emacs"),
         #     desc='Doom Emacs'
         #     ),
         # ### Switch focus to specific monitor (out of three)
         # Key([mod], "w",
         #     lazy.to_screen(0),
         #     desc='Keyboard focus to monitor 1'
         #     ),
         # Key([mod], "e",
         #     lazy.to_screen(1),
         #     desc='Keyboard focus to monitor 2'
         #     ),
         # Key([mod], "r",
         #     lazy.to_screen(2),
         #     desc='Keyboard focus to monitor 3'
         #     ),
         # ### Switch focus of monitors
         # Key([mod], "period",
         #     lazy.next_screen(),
         #     desc='Move focus to next monitor'
         #     ),
         # Key([mod], "comma",
         #     lazy.prev_screen(),
         #     desc='Move focus to prev monitor'
         #     ),
         ### Treetab controls
         Key([mod, "control"], "k",
             lazy.layout.section_up(),
             desc='Move up a section in treetab'
             ),
         Key([mod, "control"], "j",
             lazy.layout.section_down(),
             desc='Move down a section in treetab'
             ),
         ### Window controls
         Key([mod], "k",
             lazy.layout.down(),
             desc='Move focus down in current stack pane'
             ),
         Key([mod], "j",
             lazy.layout.up(),
             desc='Move focus up in current stack pane'
             ),
         Key([mod, "shift"], "k",
             lazy.layout.shuffle_down(),
             desc='Move windows down in current stack'
             ),
         Key([mod, "shift"], "j",
             lazy.layout.shuffle_up(),
             desc='Move windows up in current stack'
             ),
         Key([mod], "h",
             lazy.layout.grow(),
             lazy.layout.increase_nmaster(),
             desc='Expand window (MonadTall), increase number in master pane (Tile)'
             ),
         Key([mod], "l",
             lazy.layout.shrink(),
             lazy.layout.decrease_nmaster(),
             desc='Shrink window (MonadTall), decrease number in master pane (Tile)'
             ),
         Key([mod], "n",
             lazy.layout.normalize(),
             desc='normalize window size ratios'
             ),
         Key([mod], "m",
             lazy.layout.maximize(),
             desc='toggle window between minimum and maximum sizes'
             ),
         Key([mod, "shift"], "f",
             lazy.window.toggle_floating(),
             desc='toggle floating'
             ),
         Key([mod, "shift"], "m",
             lazy.window.toggle_fullscreen(),
             desc='toggle fullscreen'
             ),
         ### Stack controls
         Key([mod, "shift"], "space",
             lazy.layout.rotate(),
             lazy.layout.flip(),
             desc='Switch which side main pane occupies (XmonadTall)'
             ),
         Key([mod], "space",
             lazy.layout.next(),
             desc='Switch window focus to other pane(s) of stack'
             ),
         Key([mod, "control"], "Return",
             lazy.layout.toggle_split(),
             desc='Toggle between split and unsplit sides of stack'
             ),
         ### Dmenu scripts launched with ALT + CTRL + KEY
         Key(["mod1", "control"], "g",
             lazy.spawn("dmenu_lutris"),
             desc='Dmenu launcher for games'
             ),
         Key(["mod1", "control"], "m",
             lazy.spawn("./.dmenu/dmenu-sysmon.sh"),
             desc='Dmenu system monitor script'
             ),
         Key(["mod1", "control"], "p",
             lazy.spawn("passmenu2"),
             desc='Passmenu'
             ),
         Key(["mod1", "control"], "c",
             lazy.spawn("dmenu_file_history"),
             desc='Dmenu script for editing files'
             ),
         Key(["mod1", "control"], "s",
             lazy.spawn("./.dmenu/dmenu-surfraw.sh"),
             desc='Dmenu surfraw script'
             ),
         Key(["mod1", "control"], "t",
             lazy.spawn("./.dmenu/dmenu-trading.sh"),
             desc='Dmenu trading programs script'
             ),
         Key(["mod1", "control"], "i",
             lazy.spawn("./.dmenu/dmenu-scrot.sh"),
             desc='Dmenu scrot script'
             ),
         ### My applications launched with SUPER + ALT + KEY
         Key([mod, "mod1"], "b",
             lazy.spawn("brave"),
             desc='lynx browser'
             ),
         Key([mod, "mod1"], "l",
             lazy.spawn(myTerm+" -e lynx gopher://distro.tube"),
             desc='lynx browser'
             ),
         Key([mod, "mod1"], "n",
             lazy.spawn(myTerm+" -e newsboat"),
             desc='newsboat'
             ),
         Key([mod, "mod1"], "r",
             lazy.spawn(myTerm+" -e tuir"),
             desc='reddit terminal viewer'
             ),
         Key([mod, "mod1"], "e",
             lazy.spawn(myTerm+" -e neomutt"),
             desc='neomutt'
             ),
         Key([mod, "mod1"], "m",
             lazy.spawn(myTerm+" -e ncmpcpp"),
             desc='ncurses mpd client'
             ),
         Key([mod, "mod1"], "t",
             lazy.spawn(myTerm+" -e torque"),
             desc='torque transmission'
             ),
         Key([mod, "mod1"], "p",
             lazy.spawn(myTerm+" -e htop"),
             desc='htop'
             ),
         Key([mod, "mod1"], "f",
             lazy.spawn(myTerm+" -e lf"),
             desc='lf'
             ),
         Key([mod, "mod1"], "j",
             lazy.spawn(myTerm+" -e joplin"),
             desc='joplin'
             ),
         Key([mod, "mod1"], "c",
             lazy.spawn(myTerm+" -e cmus"),
             desc='cmus'
             ),
         Key([mod, "mod1"], "i",
             lazy.spawn(myTerm+" -e irssi"),
             desc='irssi'
             ),
         Key([mod, "mod1"], "y",
             lazy.spawn(myTerm+" -e youtube-viewer"),
             desc='youtube-viewer'
             ),
         Key([mod, "mod1"], "a",
             lazy.spawn(myTerm+" -e pulsemixer"),
             desc='Audio Mixer'
             ),
         Key([], 'XF86Mail', lazy.spawn(myTerm+" -e neomutt")),
         ### Media Keys
         Key([], 'XF86AudioMute', lazy.spawn('pamixer --toggle-mute')),
         Key([], 'XF86AudioLowerVolume', lazy.spawn('pamixer --allow-boost -d 5')),
         Key([], 'XF86AudioRaiseVolume', lazy.spawn('pamixer --allow-boost -i 5')),
         Key([mod], 'XF86AudioLowerVolume', lazy.spawn('mpc volume -5')),
         Key([mod], 'XF86AudioRaiseVolume', lazy.spawn('mpc volume +5')),
         Key([], 'XF86AudioPlay', lazy.spawn('mpc toggle')),
         Key([], 'XF86AudioStop', lazy.spawn('mpc stop')),
         Key([], 'XF86AudioPrev', lazy.spawn('mpc prev')),
         Key([], 'XF86AudioNext', lazy.spawn('mpc next')),
]

group_names = [("1", {'layout': 'monadtall', "matches": [Match(wm_instance_class=["brave-browser", "qutebrowser"])]}),
               ("2", {'layout': 'monadtall', "matches": [Match(wm_instance_class=["pcmanfm"])]}),
               ("3", {'layout': 'monadtall'}),
               ("4", {'layout': 'monadtall'}),
               ("5", {'layout': 'monadwide'}),
               ("6", {'layout': 'monadtall'}),
               ("7", {'layout': 'monadtall', "matches": [Match(wm_instance_class=["gimp"])]}),
               ("8", {'layout': 'monadtall'}),
               ("9", {'layout': 'monadtall', "matches": [Match(wm_instance_class=["Steam"])]})]

groups = [Group(name, **kwargs) for name, kwargs in group_names]

for i, (name, kwargs) in enumerate(group_names, 1):
    keys.append(Key([mod], str(i), lazy.group[name].toscreen()))        # Switch to another group
    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(name))) # Send current window to another group

layout_theme = {"border_width": 2,
                "margin": 2,
                "border_focus": "d79921",
                "border_normal": "484848"
                }

layouts = [
    layout.MonadWide(**layout_theme),
    #layout.Bsp(**layout_theme),
    #layout.Stack(stacks=2, **layout_theme),
    #layout.Columns(**layout_theme),
    #layout.RatioTile(**layout_theme),
    # layout.VerticalTile(**layout_theme),
    #layout.Matrix(**layout_theme),
    #layout.Zoomy(**layout_theme),
    layout.MonadTall(**layout_theme),
    layout.Max(**layout_theme),
  # layout.Tile(shift_windows=True, **layout_theme),
    # layout.Stack(num_stacks=2),
    # layout.TreeTab(
    #      font = "Ubuntu",
    #      fontsize = 10,
    #      sections = ["FIRST", "SECOND"],
    #      section_fontsize = 11,
    #      bg_color = "141414",
    #      active_bg = "90C435",
    #      active_fg = "000000",
    #      inactive_bg = "384323",
    #      inactive_fg = "a0a0a0",
    #      padding_y = 5,
    #      section_top = 10,
    #      panel_width = 320
    #      ),
    layout.Floating(**layout_theme)
]

colors = [["#282828", "#282828"], # panel background 0
          ["#484848", "#484848"], # background for current screen tab 1
          ["#ebdbb2", "#ebdbb2"], # font color for group names 2
          ["#cc241d", "#cc241d"], # border line color for current tab 3
          ["#b16286", "#b16286"], # border line color for other tab and odd widgets 4
          ["#458588", "#458588"], # color for the even widgets 5
          ["#b8bb26", "#b8bb26"], # window name 6
          ["#d79921", "#d79921"]] #

prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())

##### DEFAULT WIDGET SETTINGS #####
widget_defaults = dict(
    font="Sans Bold",
    fontsize = 12,
    padding = 2,
    background=colors[2]
)
extension_defaults = widget_defaults.copy()

def init_widgets_list():
    widgets_list = [
              widget.Sep(
                       linewidth = 0,
                       padding = 6,
                       foreground = colors[2],
                       background = colors[0]
                       ),
              # widget.Image(
              #          filename = "~/.config/qtile/icons/python.png",
              #          mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn('dmenu_run_history')}
              #          ),
              widget.GroupBox(
                       font = "Sans Bold",
                       fontsize = 10,
                       margin_y = 3,
                       margin_x = 0,
                       padding_y = 5,
                       padding_x = 3,
                       borderwidth = 3,
                       active = colors[2],
                       inactive = colors[2],
                       disable_grab = True,
                       rounded = False,
                       hide_unused = True,
                       highlight_color = colors[1],
                       highlight_method = "block",
                       this_current_screen_border = colors[1],
                       this_screen_border = colors [4],
                       other_current_screen_border = colors[0],
                       other_screen_border = colors[0],
                       foreground = colors[2],
                       background = colors[0]
                       ),
              widget.Prompt(
                       prompt = prompt,
                       font = "Monospace",
                       padding = 10,
                       foreground = colors[3],
                       background = colors[1]
                       ),
              widget.Sep(
                       linewidth = 0,
                       padding = 4,
                       foreground = colors[2],
                       background = colors[0]
                       ),
              widget.CurrentLayoutIcon(
                       custom_icon_paths = [os.path.expanduser("~/.config/qtile/icons")],
                       foreground = colors[2],
                       background = colors[0],
                       padding = 2,
                       scale = 0.7
                       ),
              # widget.CurrentLayout(
              #          foreground = colors[2],
              #          background = colors[0],
              #          padding = 5
              #          ),
              # widget.WindowName(
              #          foreground = colors[6],
              #          background = colors[0],
              #          padding = 2
              #          ),
              widget.Mpd2(
                       keys = {'toggle': 1},
                       font = "Sans Bold Italic",
                       foreground = colors[6],
                       background = colors[0],
                       fontsize = 11,
                       no_connection = " ",
                       idle_format = '',
                       status_format = '{title} by {artist}'
                       ),
              widget.Spacer(
                       background = colors[0]
                       ),
              # widget.TextBox(
              #          text = " ₿",
              #          padding = 0,
              #          foreground = colors[2],
              #          background = colors[4],
              #          fontsize = 12
              #          ),
              # widget.BitcoinTicker(
              #          foreground = colors[2],
              #          background = colors[4],
              #          padding = 5
              #          ),
              # widget.TextBox(
              #          text = " 🌡",
              #          padding = 2,
              #          foreground = colors[2],
              #          background = colors[5],
              #          fontsize = 11
              #          ),
              # widget.ThermalSensor(
              #          foreground = colors[2],
              #          background = colors[5],
              #          threshold = 90,
              #          padding = 5
              #          ),
              # widget.TextBox(
              #          text = " ⟳",
              #          padding = 2,
              #          foreground = colors[3],
              #          background = colors[0],
              #          fontsize = 14
              #          ),
              # widget.Pacman(
              #          update_interval = 1800,
              #          foreground = colors[3],
              #          mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn(myTerm + ' -e sudo pacman -Syu')},
              #          background = colors[0]
              #          ),
              # widget.TextBox(
              #          text = "Updates",
              #          padding = 5,
              #          mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn(myTerm + ' -e sudo pacman -Syu')},
              #          foreground = colors[3],
              #          background = colors[0]
              #          ),
              widget.GenPollText(
                       foreground = colors[3],
                       background = colors[0],
                       padding = 4,
                       update_interval = 30,
                       func=lambda: subprocess.check_output(scriptfolder + "downloads").decode("utf-8").replace('\n', ''),
                       mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn(myTerm + ' -e dlmgr -e')},
                       ),
              widget.TextBox(
                       text = "",
                       foreground = colors[4],
                       background = colors[0],
                       padding = 0,
                       fontsize = 14
                       ),
              widget.Memory(
                       foreground = colors[4],
                       background = colors[0],
                       mouse_callbacks = {'Button1': lambda qtile: qtile.cmd_spawn(myTerm + ' -e htop')},
                       padding = 5
                       ),
              widget.Net(
                       interface = "wlp0s29f7u4",
                       format = '↓{down} ↑{up}',
                       foreground = colors[5],
                       background = colors[0],
                       padding = 5
                       ),
              widget.TextBox(
                      text = "墳",
                       foreground = colors[6],
                       background = colors[0],
                       padding = 0
                       ),
              widget.PulseVolume(
                       foreground = colors[6],
                       background = colors[0],
                       padding = 5
                       ),
              widget.Clock(
                       foreground = colors[7],
                       background = colors[0],
                       format = " %a %d/%m %I:%M%p"
                       ),
              widget.Sep(
                       linewidth = 0,
                       padding = 5,
                       foreground = colors[7],
                       background = colors[0]
                       ),
              widget.Systray(
                       background = colors[0],
                       padding = 5
                       ),
              ]
    return widgets_list

def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    return widgets_screen1                       # Slicing removes unwanted widgets on Monitors 1,3

def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    return widgets_screen2                       # Monitor 2 will display all widgets in widgets_list

def init_screens():
    return [Screen(top=bar.Bar(widgets=init_widgets_screen1(), opacity=1.0, size=20)),
            Screen(top=bar.Bar(widgets=init_widgets_screen2(), opacity=1.0, size=20)),
            Screen(top=bar.Bar(widgets=init_widgets_screen1(), opacity=1.0, size=20))]

if __name__ in ["config", "__main__"]:
    screens = init_screens()
    widgets_list = init_widgets_list()
    widgets_screen1 = init_widgets_screen1()
    widgets_screen2 = init_widgets_screen2()

def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)

def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)

def window_to_previous_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i != 0:
        group = qtile.screens[i - 1].group.name
        qtile.current_window.togroup(group)

def window_to_next_screen(qtile):
    i = qtile.screens.index(qtile.current_screen)
    if i + 1 != len(qtile.screens):
        group = qtile.screens[i + 1].group.name
        qtile.current_window.togroup(group)

def switch_screens(qtile):
    i = qtile.screens.index(qtile.current_screen)
    group = qtile.screens[i - 1].group
    qtile.current_screen.set_group(group)

mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False

floating_layout = layout.Floating(float_rules=[
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'Textures'},
    {'wmclass': 'Pinentry-gtk-2'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
    {'wmclass': 'MEGAsync'},
    {'wmclass': 'hl2_linux'},
])
auto_fullscreen = True
focus_on_window_activation = "focus"

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
