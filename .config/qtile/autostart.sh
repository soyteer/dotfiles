#!/bin/sh
export XDG_SESSION_TYPE="X11"
export XDG_CURRENT_DESKTOP="gnome"

# playaudio ~/.local/share/sounds/metro_id.mp3 >/dev/null &
playaudio /usr/share/sounds/freedesktop/stereo/service-login.oga &
# setxkbmap br &                          # ABNT-2 keyboard layout
setxkbmap -option keypad:pointerkeys &  # use keypad as a cursor, activate with Shift + Num Lock
xrdb -load ~/.config/x/Xresources       # configs for X
xset r rate 300 50                      # speed up the key presses
lxpolkit &                              # polkit authenticator
hsetroot -cover $HOME/.config/wall      # wallpaper
dunst &                                 # notifications
parcellite &                            # clipboard
! pgrep mpd && mpd &                    # mpd music server
# killall -q redshift; redshift &         # change color at night
# picom &                                 # screen compositor
# ! pgrep syncthing && syncthing &        # sync with the phone
# megasync &                              # sync with the cloud
# print-updates &                         # show some update available
killall -q xidlehook; idlelock &        # lock screen (xidlehook and betterlockscreen+i3lock)
unclutter &                             # hides cursor when idle
