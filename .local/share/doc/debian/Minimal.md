# Debian Minimal Setup

A basic debian installation can take a lot of space without even noticing, and here are some configurations to help it.

When installing packages with `apt(1)` using the flag `--no-install-recommends` will not install unnecessary packages, making the system more manageable.

Some aliases to help with apt

    alias api='sudo apt install --no-install-recommends'
    alias apr='sudo apt purge'
    alias apu='sudo apt update'
    alias apq='apt search'
    alias apl='apt list --installed'
    alias apro='sudo apt autoremove'
    alias apqf='dpkg-query -L'

### Core applications

git - for file version control
neovim - text editor
sudo - root access

### Sudo Setup

Debian doesn't offer the `wheel` group commonly used on other linux distributions, instead the group `sudo` should be used.

First add the group into your user

    usermod -aG sudo *user*

Then re-login and check if you can have sudo privileges.

### X Server configuration

xserver-xorg-core - offers the bare minimal xorg
xserver-xorg-input-all - input drivers for X
xserver-xorg-video-*graphicscard* - video drivers for X Server intel,amdgpu,vmware...
xinit - start X session
xterm - terminal emulator
x11-xserver-utils - xrandr, xset, xgamma, xsetroot, xrdb...
x11-apps - xcalc, xclock, xload, xlogo, oclock...
x11-utils - xprop, xmessage, xev, xkill...
