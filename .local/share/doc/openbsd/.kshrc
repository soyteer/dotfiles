#. /etc/ksh.kshrc

PS1="[\u@\h \w]$ "

set -o emacs

HISTFILE="$HOME/.ksh_history"
HISTSIZE="10000"

alias ls="ls -F"
alias la="ls -lAh"
alias cls="clear"
alias quit="exit"

alias pki="doas pkg_add"
alias pkq="pkg_info -Q"
alias pkr="doas pkg_delete"
