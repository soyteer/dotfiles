#!/bin/sh

cachedir=${XDG_CACHE_HOME:-"$HOME/.cache"}
if [ -d "$cachedir" ]; then
	cache=$cachedir/dmenu_files
	historyfile=$cachedir/dmenu_file_history
else			# if no xdg dir, fall back to dotfiles in ~
	cache=$HOME/.dmenu_files
	historyfile=$HOME/.dmenu_file_history
fi

CONFIGPATH="$HOME/prj/git/dotfiles/.local/sbin
$HOME/.local/bin
$HOME/prj/git/dotfiles/.config"


if [ ! -f "$cache" ] || [ "$(stat -c %y "$cache" 2>/dev/null | cut -f1)" = "$(date '+%Y-%m-%d')" ]; then
  for f in $CONFIGPATH; do
    find "$f" -type f -exec file -F\  --mime-type '{}' \; | grep "text/.*" | awk '{print $1}' | sort >> "$cache"
  done
  # Run on the HOME directory but not recursively
  find "$HOME/prj/git/dotfiles" -maxdepth 1 -type f -exec file -F\  --mime-type '{}' \; | grep "text/.*" | awk '{print $1}' | sort >> "$cache"
fi

awk -v histfile=$historyfile '
	BEGIN {
		while( (getline < histfile) > 0 ) {
			sub("^[0-9]+\t","")
			print
			x[$0]=1
		}
	} !x[$0]++ ' "$cache" \
	| dmenu -F -i -c -bw 2 -l 20 -p "Files" "$@" \
	| awk -v histfile=$historyfile '
		BEGIN {
			FS=OFS="\t"
			while ( (getline < histfile) > 0 ) {
				count=$1
				sub("^[0-9]+\t","")
				fname=$0
				history[fname]=count
			}
			close(histfile)
		}

		{
			history[$0]++
			print
		}

		END {
			if(!NR) exit
			for (f in history)
				print history[f],f | "sort -t '\t' -k1rn >" histfile
		}
	' \
	| while read cmd; do ${SHELL:-"/bin/sh"} -c "open-term ${EDITOR:-vi} $cmd" & done
