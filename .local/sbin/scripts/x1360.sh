#!/bin/sh

pgrep -x sway && swaymsg output VGA-1 res 1366x768 ||
    xrandr --output VGA-1 --primary --mode 1360x768 --pos 0x0 --rotate normal --output VIRTUAL1 --off
pgrep -x pcmanfm && pcmanfm --desktop-off && sleep 2 &&
  setsid pcmanfm --desktop &
