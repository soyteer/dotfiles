#!/bin/sh

pgrep -x sway && swaymsg output VGA-1 res 800x600 ||
    xrandr --output VGA-1 --primary --mode 800x600 --pos 0x0 --rotate normal --output VIRTUAL1 --off
