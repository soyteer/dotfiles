#!/bin/sh

pgrep -x sway && swaymsg output VGA-1 res 640x480 ||
    xrandr --output VGA-1 --primary --mode 640x480 --pos 0x0 --rotate normal --output VIRTUAL1 --off
