#!/usr/bin/env zsh

export LANG="en_US.UTF-8"

PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/games:/usr/games"
PATH="$(find "$HOME/.local/sbin/" -type d | paste -sd ':'):$HOME/.local/bin:$PATH"
export PATH

export EDITOR="nvim"
export TERMINAL="st"
export BROWSER="librewolf"
export FILE="open-term lf"
export MANPAGER="sh -c 'col -bx | bat -l man -p'"

# XDG Stuff
export XDG_MENU_PREFIX="gnome-"
export XDG_SESSION_TYPE="tty"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_FAKE_HOME="$XDG_DATA_HOME/fake-home"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_RUNTIME_DIR="${XDG_RUNTIME_DIR:-"/tmp/xdg-$USER"}"
[ ! -d "$XDG_RUNTIME_DIR" ] && mkdir -p "$XDG_RUNTIME_DIR"

# Away from $HOME!
export ANDROID_SDK_HOME="$XDG_CONFIG_HOME/android"
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc-2.0"
export ZDOTDIR="$XDG_CONFIG_HOME/zsh"
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME/npm/npmrc"
export LESSHISTFILE="-"
export INPUTRC="$XDG_CONFIG_HOME/readline/inputrc"
export WGETRC="$XDG_CONFIG_HOME/wget/wgetrc"
export XAUTHORITY="$XDG_RUNTIME_DIR/Xauthority"; ln -sf "$XAUTHORITY" "$HOME/.Xauthority"
export ICEAUTHORITY="$XDG_RUNTIME_DIR/ICEauthority"
export PASSWORD_STORE_DIR="$XDG_DATA_HOME/pass"
export MBSYNCRC="$XDG_CONFIG_HOME/mbsync/mbsyncrc"
export NOTMUCH_CONFIG="$XDG_CONFIG_HOME/notmuch-config"
export WINEPREFIX="$XDG_DATA_HOME/wineprefixes/default"
[ ! -d "$WINEPREFIX" ] && mkdir -p "$WINEPREFIX"

# GPG Configuration
gpgsetup() {
  command -v gpg >/dev/null &&
  mkdir -p "$GNUPGHOME" &&
  chmod 700 "$GNUPGHOME" &&
  touch "$GNUPGHOME/gpg.conf"
  chmod 600 "$GNUPGHOME/gpg.conf"
}
export GNUPGHOME="$XDG_DATA_HOME/gnupg"
[ ! -d "$GNUPGHOME" ] && gpgsetup
GPG_TTY=$(tty)
export GPG_TTY
unset -f gpgsetup

# Etc.
#IFWIFI="$(printf '%s\n' /sys/class/net/wl*)"
#export IFWIFI
export QT_QPA_PLATFORMTHEME="qt5ct"
export DOOMWADDIR="$HOME/game/doom"
export SDL_SOUNDFONTS="$HOME/.config/dosbox/soundfonts/SC-55.SoundFont.v1.2b.sf2"
export WALLDIR="$HOME/img/backgrounds"
export TUIR_BROWSER="webopener"
export SUDO_ASKPASS="$HOME/.local/sbin/dmenu/dmenupass"
export YTDL_VIDPRESET="302+251|mp4[height<=720]+m4a/best[height<=720]+bestaudio/720p/mp4/best" # 720p

# Fix font rendering in Java apps
export _JAVA_OPTIONS="-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java -Dawt.useSystemAAFontSettings=on -Dswing.aatext=true"
export AWT_TOOLKIT="MToolkit wmname LG3D"	# May have to install wmname
export _JAVA_AWT_WM_NONREPARENTING=1	# Fix for Java applications in dwm
[ ! -d "$XDG_CONFIG_HOME/java" ] && mkdir -p "$XDG_CONFIG_HOME/java"

eval $(/usr/bin/dircolors)

# Less with Colors
# export LESS=-R
# export LESS_TERMCAP_mb="$(printf '%b' '[1;31m')"
# export LESS_TERMCAP_md="$(printf '%b' '[1;36m')"
# export LESS_TERMCAP_me="$(printf '%b' '[0m')"
# export LESS_TERMCAP_so="$(printf '%b' '[01;44;33m')"
# export LESS_TERMCAP_se="$(printf '%b' '[0m')"
# export LESS_TERMCAP_us="$(printf '%b' '[1;32m')"
# export LESS_TERMCAP_ue="$(printf '%b' '[0m')"
# export LESSOPEN="| /usr/bin/highlight -O ansi %s 2>/dev/null"

homecleaner	# Script to clear some unwanted files and directories

# First login setup. Is this really necessary?
if [ ! -f "/tmp/1st.login" ]; then
  touch /tmp/1st.login
  setsid -f rssreader --force-update >/dev/null 2>&1
  WM=jwm
  printf '\n'
  tail ~/doc/notes 2>/dev/null
  calcurse -d3 2>/dev/null
  printf '\nStart %s? [Y/n] ' "$WM" && read -r ans
  [ -z "$ans" ] || [ "$ans" = "y" ] || [ "$ans" = "Y" ] && {
  echo -en "\e]P0282828"
  fbv -ifka -s15 "$HOME/.local/share/doc/tux.png"
  x "$WM"
  }
fi
#x jwm
